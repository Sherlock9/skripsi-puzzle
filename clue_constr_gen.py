# 1. Generates the spots that the clues fill
# 2. Generates the clue constraints

from collections import namedtuple
import copy


def single_cell_check_func(cells):
    return cells[0]["value"]


def value_count(cells, value):
    return len([cell["value"] == value for cell in cells])


class Clueconstrgen:
    def __init__(self, solution, grid_type, region_spec, clue_distrib, domain, constraints):
        self.solution = copy.deepcopy(solution)
        self.grid_type = copy.deepcopy(grid_type)
        self.region_spec = copy.deepcopy(region_spec)
        self.clue_distrib = copy.deepcopy(clue_distrib)
        self.constraints = copy.deepcopy(constraints)
        self.domain = domain
        self.valid_values = [self.domain[key]
                             for key in self.domain.keys() if key != "default"]
        self.clue_constraints = {}

        # calc_valid_assign
        self.valid_assignments = {}

        self.Clue = namedtuple(
            "Clue", ["region", "check_func", "expected_value"])

        self.generate_clue_positions()
        self.calc_valid_assign()

    def generate_clue_positions(self):
        if "edge" in self.clue_distrib:
            if "los" in self.region_spec and "edge" in self.region_spec["los"]["direction"]:
                los_rules = self.region_spec["los"]["rules"]
                edge_regions = []
                self.clue_constraints["edge"] = []
                if "rect" in self.grid_type:
                    neighbors = [(0, -1), (-1, 0), (0, 1), (1, 0)]
                    width = self.grid_type["rect"]["width"]
                    height = self.grid_type["rect"]["height"]
                    for direction in range(4):
                        if direction % 2 == 1:
                            if direction // 2 == 1:
                                start = 0
                            else:
                                start = width - 1
                            for p in range(width):
                                region = self.edge_los(
                                    width, height, start, p, neighbors, direction)
                                edge_regions.append(region)
                        else:
                            if direction // 2 == 1:
                                start = 0
                            else:
                                start = height - 1
                            for q in range(height):
                                region = self.edge_los(
                                    width, height, q, start, neighbors, direction)
                                edge_regions.append(region)
                for region in edge_regions:
                    cells = [self.solution[i] for i in region]
                    for constr in los_rules:
                        expected_value = constr(cells)
                        new_constr = self.Clue(region, constr, expected_value)
                        self.clue_constraints["edge"].append(new_constr)

        if "cell" in self.clue_distrib:
            valid_values = self.clue_distrib["cell"]["values"]
            self.clue_constraints["cell"] = []
            if "los" in self.region_spec and "cell" in self.region_spec["los"]["direction"]:
                los_rules = self.region_spec["los"]["rules"]
                cell_clue_regions = []
                if "rect" in self.grid_type:
                    neighbors = [(0, -1), (-1, 0), (0, 1), (1, 0)]
                    width = self.grid_type["rect"]["width"]
                    height = self.grid_type["rect"]["height"]
                    for i in range(1, len(self.solution)):
                        region = self.cell_los(
                            width, height, i, neighbors, valid_values)
                        cell_clue_regions.append(region)
                for region in cell_clue_regions:
                    cells = [self.solution[i] for i in region]
                    for constr in los_rules:
                        expected_value = constr(cells)
                        new_constr = self.Clue(
                            region, constr, expected_value)
                        self.clue_constraints["cell"].append(new_constr)
            else:
                for cell_num in range(1, len(self.solution)):
                    region = [cell_num]
                    constr = single_cell_check_func
                    expected_value = self.solution[cell_num]["value"]
                    if expected_value in valid_values:
                        new_constr = self.Clue(
                            region, constr, expected_value)
                        self.clue_constraints["cell"].append(new_constr)

        if "count" in self.clue_distrib:
            self.clue_constraints["count"] = []
            region = tuple(range(len(self.solution)))
            constr = value_count
            for value in self.clue_distrib["count"]:
                expected_value = value_count(self.solution, value)
                new_constr = self.Clue(region, constr, expected_value)
                self.clue_constraints["count"].append(new_constr)

        if "pair" in self.region_spec and "pair" in self.clue_distrib:
            pair_rules = self.region_spec["pair"]["rules"]
            self.clue_constraints["pair"] = []
            if "rect" in self.grid_type:
                neighbors = []
                # half the neighbors to prevent duplicates
                if self.clue_distrib["pair"]["adjacency"] == "edge":
                    neighbors = [(0, 1), (1, 0)]
                elif self.clue_distrib["pair"]["adjacency"] == "vertex":
                    neighbors = [(0, 1), (1, -1), (1, 0), (1, 1)]
                width = self.grid_type["rect"]["width"]
                height = self.grid_type["rect"]["height"]
                for cell_num in range(1, len(self.solution)):
                    x, y = (cell_num - 1) // width, (cell_num - 1) % width
                    for nei in neighbors:
                        a, b = nei
                        other_cell = (x + b) * width + y + a + 1
                        if 0 <= x + b < width and 0 <= y + a < height:
                            for constr in pair_rules:
                                region = [cell_num, other_cell]
                                cells = [self.solution[i] for i in region]
                                expected_value = constr(cells)
                                new_constr = self.Clue(
                                    region, constr, expected_value)
                                self.clue_constraints["pair"].append(
                                    new_constr)

    def edge_los(self, width, height, line, line_start, neighbors, start_direction):
        region = []
        direction = start_direction
        still_adding = True
        i = line * width + line_start
        while still_adding:
            region.append(i+1)
            x, y = i % width, i // width
            a, b = neighbors[direction]
            if 0 <= x + b < width and 0 <= y + a < height:
                i = (y + a) * width + x + b
            else:
                still_adding = False
        return region

    def cell_los(self, width, height, cell_num, neighbors, valid_values):
        region = []
        x, y = (cell_num - 1) % width, (cell_num - 1) // width
        for direction in range(len(neighbors)):
            m = 1
            still_adding = True
            a, b = direction
            while still_adding:
                this_cell = (x + b * m) * width + y + a * m + 1
                this_value = self.solution[cell_num]["value"]
                if 0 <= x + b * m < height and 0 <= y + a * m < width \
                        and this_value in valid_values:
                    region.append(this_cell)
                else:
                    still_adding = False
        return region

    def calc_valid_assign(self):
        # Calculates all sets of cell assignments that satisfy
        # the non-clue constraints
        # and store them by their clue constraint values
        non_clue_constraints = set()
        for item in self.constraints:
            for constr_list in item:
                for constr in constr_list:
                    non_clue_constraints.add(constr.check_func)
        for key in self.clue_constraints:
            if key != "count":
                constr_list = self.clue_constraints[key]
                # all the values expected for each function
                assignment_set = {}
                value_set = {(constr.expected_value, constr.check_func)
                             for constr in constr_list}
                for constr in constr_list:
                    len_reg = len(constr.region)
                    check = constr.check_func
                    domains = []
                    for j in range(len_reg):
                        domains.append(copy.deepcopy(self.valid_values))
                    index = 0
                    combo = [self.domain["default"] for _ in range(len_reg)]
                    while index >= 0:
                        combo[index] = domains[index][0]
                        if all(ncc(combo) for ncc in non_clue_constraints):
                            if index == len_reg - 1:
                                assign = tuple(combo)
                                value = check(assign)
                                if (value, check) in value_set:
                                    new_key = str(len_reg) + \
                                        str(value) + str(check)
                                    if new_key in assignment_set:
                                        assignment_set[new_key].append(assign)
                                    else:
                                        assignment_set[new_key] = [assign]
                                domains[index].pop(0)
                            else:
                                index += 1
                        else:
                            domains[index].pop(0)
                        while len(domains[index]) == 0 and index >= 0:
                            for j in range(index, len_reg):
                                domains[j] = copy.deepcopy(self.valid_values)
                                combo[j] = self.domain["default"]
                            domains[index-1].pop(0)
                            index -= 1
                self.valid_assignments[key] = assignment_set

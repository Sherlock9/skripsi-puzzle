# Solgen implements the FC-CBJ algorithm outlined by Prosser
# We also add constraints that can only be added to the puzzle
# after the solution has been generated

# 1. Shuffle spots and assign some portion of spots as clues
# NOTE for Ch. 5; clue assignment is very crude, can be improved
# 2. Calculate domains by the sets of cells whose CCVs they match
# 3. solve the puzzle by whittling down the domains to the single
# remaining candidate (SRC) which becomes that cell's assignment
# or by removing candidate values that conflict with recent SRC assignments
# 4. If current clues are insufficient, add another clue
# 5. Return when solveable
# NOTE for Ch. 5; in future, can add the following:
# if starting clues are enough, remove some clues to increase difficulty

import copy
import random


class Initgen:
    def __init__(self, domain, solution, constraints, clue_constraints, clue_distrib, valid_assignments):
        self.domain = copy.deepcopy(domain)
        self.solution = copy.deepcopy(solution)
        self.constraints = copy.deepcopy(constraints)
        self.clue_constraints = copy.deepcopy(clue_constraints)
        self.clue_distrib = copy.deepcopy(clue_distrib)
        self.valid_assignments = copy.deepcopy(valid_assignments)

        self.status = "unknown"

        # shuffle_clues
        # NOTE spot_shuffle not strictly necessary since current_clues
        # and remaining_clue_options preserve this shuffled order
        # May be useful for user debugging, though
        self.spot_shuffle = {}
        self.current_clues = {}
        self.remaining_clue_options = {}

        # create_assignments_and_domains
        self.valid_values = [self.domain[key]
                             for key in self.domain.keys() if key != "default"]
        self.prev_assignments = {}
        self.curr_assignments = {}
        self.prev_domains = []
        self.curr_domains = []

        self.shuffle_clues()
        self.create_assignments_and_domains()
        self.verify_loop()

    def shuffle_clues(self):
        # Shuffles all the clue constraints and assigns
        # a predefined portion (in clue_distrib) of the constraints as clues
        # (except the value counts which are never partial clues)
        nums = {}
        for key in self.clue_distrib.keys():
            if key != "count":
                nums[key] = int(self.clue_distrib[key]["proportion"]
                                * len(self.clue_constraints[key]))
        if all(num == 0 for num in nums.values()):
            clue_key = [key for key in nums.keys() if key != "count"][0]
            nums[clue_key] = 1
        for key in self.clue_distrib.keys():
            # the only type of clue that does not need shuffling
            if key == "count":
                self.current_clues["count"] = self.clue_constraints["count"]
                # still instantiating remaining_clue_options to avoid a KeyError
                self.remaining_clue_options["count"] = []
            else:
                constraints = copy.deepcopy(self.clue_constraints[key])
                shuffle = list(range(len(constraints)))
                random.shuffle(shuffle)
                self.spot_shuffle[key] = copy.deepcopy(shuffle)

                num = nums[key]
                clues_used = shuffle[:num]
                clues_unused = shuffle[num:]

                self.current_clues[key] = []
                self.remaining_clue_options[key] = []
                for constr_index in clues_used:
                    constr = copy.deepcopy(
                        self.clue_constraints[key][constr_index])
                    self.current_clues[key].append(constr)
                for constr_index in clues_unused:
                    constr = copy.deepcopy(
                        self.clue_constraints[key][constr_index])
                    self.remaining_clue_options[key].append(constr)

    def create_assignments_and_domains(self):
        cell_assignments = {}
        cell_domains = []    # [list of lists of values]
        for _ in range(len(self.solution)):
            cell_domains.append(copy.deepcopy(self.valid_values))
        for key in self.current_clues.keys():
            for cc in self.current_clues[key]:
                cc_domains = []
                for _ in range(len(self.solution)):
                    cc_domains.append([])
                assign_key = str(len(cc.region)) + \
                    str(cc.expected_value) + str(cc.check_func)
                region = tuple(cc.region)
                for assign in self.valid_assignments[key][assign_key]:
                    # assignments
                    if region in cell_assignments:
                        cell_assignments[region].append(assign)
                    else:
                        cell_assignments[region] = [assign]
                    # domains
                    for index, value in enumerate(assign):
                        cell_num = region[index]
                        cc_domains[cell_num].append(value)
                for domain_key in range(len(cc_domains)):
                    new_domain = []
                    for item in cell_domains[domain_key]:
                        if item in cc_domains[domain_key]:
                            new_domain.append(item)
                    cell_domains[domain_key] = copy.deepcopy(new_domain)
        # in case the clue assignments leave some domains blank at the start
        for domain_key in range(1, len(cell_domains)):
            if len(cell_domains[domain_key]) == 0:
                cell_domains[domain_key] = copy.deepcopy(self.valid_values)
        self.prev_assignments = copy.deepcopy(cell_assignments)
        self.curr_assignments = copy.deepcopy(cell_assignments)
        self.prev_domains = copy.deepcopy(cell_domains)
        self.curr_domains = copy.deepcopy(cell_domains)

    def verify_loop(self):
        while self.status == "unknown":
            product_of_domain_lengths = 1
            self.filter_assignments()
            self.filter_domains()

            # check if the assignments have changed
            for key in self.curr_assignments.keys():
                if len(self.prev_assignments[key]) != len(self.curr_assignments[key]):
                    self.status = "unknown"
                    break
            else:
                self.status = "maybe"

            # check if the domains have changed
            for index, item in enumerate(self.curr_domains):
                if index > 0:
                    product_of_domain_lengths *= len(item)
                if len(self.prev_domains[index]) != len(item):
                    self.status = "unknown"
                    break
            else:
                # if the assignments and domains haven't changed
                if self.status == "maybe":
                    # check if we've found
                    # a) a unique solution;
                    if product_of_domain_lengths == 1:
                        self.status = "solution"
                    # b) more than one solution,
                    # in which case we need more clues; or
                    elif product_of_domain_lengths > 1:
                        self.status = "unknown"
                        self.add_more_clues()
                    # c) no solutions;
                    elif product_of_domain_lengths == 0:
                        self.status = "impossible"

            # if the assignments and domains are still changing
            # or if we've just added more clues
            # store the current assignments and domains
            # and continue the while loop
            if self.status == "unknown":
                for key in self.curr_assignments.keys():
                    self.prev_assignments[key] = copy.deepcopy(
                        self.curr_assignments[key])
                for index, item in enumerate(self.curr_domains):
                    self.prev_domains[index] = copy.deepcopy(item)

    def filter_assignments(self):
        for region in self.curr_assignments.keys():
            kept_assignments = []
            for assign in self.curr_assignments[region]:
                keep_assign = True
                for index, cell_num in enumerate(region):
                    if assign[index] not in self.curr_domains[cell_num]:
                        keep_assign = False
                if keep_assign and assign not in kept_assignments:
                    kept_assignments.append(assign)
            self.curr_assignments[region] = kept_assignments

    def filter_domains(self):
        for region in self.curr_assignments.keys():
            for index, cell_num in enumerate(region):
                new_domain = []
                kept_domain = []
                for assign in self.curr_assignments[region]:
                    if assign[index] not in new_domain:
                        new_domain.append(assign[index])
                for item in new_domain:
                    if item in self.curr_domains[cell_num]:
                        kept_domain.append(item)
                self.curr_domains[cell_num] = kept_domain

    def add_more_clues(self):
        clues_left = any(
            value for value in self.remaining_clue_options.values())
        if not clues_left:
            self.status = "impossible"
            return

        # the amount of errors is the amount of extra values
        # i.e. the size of the domain - 1
        errant_cells = [len(cell_domain) - 1 for cell_domain in self.curr_domains]
        good_clue_options = {}
        for key in self.remaining_clue_options.keys():
            good_clue_options[key] = {0: []}
            for constr in self.remaining_clue_options[key]:
                error_count = 0
                for cell_num in constr.region:
                    error_count += errant_cells[cell_num]
                if error_count in good_clue_options[key]:
                    good_clue_options[key][error_count].append(constr)
                else:
                    good_clue_options[key][error_count] = [constr]

        # choosing between the four different types of clues
        # by picking the one which will
        # affect the largest number of errant cells
        maximum_error_count = 0
        new_clue_key = ""
        for key in good_clue_options.keys():
            if max(good_clue_options[key].keys()) > maximum_error_count:
                maximum_error_count = max(good_clue_options[key].keys())
                new_clue_key = key

        selected_clue = good_clue_options[new_clue_key][maximum_error_count][0]
        self.remaining_clue_options[new_clue_key].remove(selected_clue)
        self.current_clues[new_clue_key].append(selected_clue)

        # adding the effects of the clue to curr_assignments and curr_domains
        cc_domains = {}
        assign_key = str(len(selected_clue.region)) + \
            str(selected_clue.expected_value) + str(selected_clue.check_func)
        region = tuple(selected_clue.region)
        for assign in self.valid_assignments[new_clue_key][assign_key]:
            # assignments
            if region in self.curr_assignments:
                self.curr_assignments[region].append(assign)
            else:
                self.curr_assignments[region] = [assign]
            # domains
            for index, value in enumerate(assign):
                cell_num = region[index]
                if cell_num in cc_domains:
                    if value not in cc_domains[cell_num]:
                        cc_domains[cell_num].append(value)
                else:
                    cc_domains[cell_num] = [value]
        for domain_key in cc_domains:
            new_domain = []
            if self.curr_domains[domain_key]:
                for item in self.curr_domains[domain_key]:
                    if item in cc_domains[domain_key]:
                        new_domain.append(item)
            else:
                new_domain = copy.deepcopy(cc_domains[domain_key])
            self.curr_domains[domain_key] = copy.deepcopy(new_domain)

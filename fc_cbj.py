# Implements FC-CBJ algorithm described in Prosser 1993
# considering solgen and initgen need specialized versions of this algorithm
# this is largely a test implementation

import copy


class FCCBJ:
    def __init__(self, cells, regions, domain, constraints):
        self.cells = copy.deepcopy(cells)
        self.regions = copy.deepcopy(regions)
        self.domain = copy.deepcopy(domain)
        self.constraints = copy.deepcopy(constraints)
        self.consistent = True
        self.status = "unknown"
        self.original_domain = []   # [cell_index: list of value]
        self.current_domain = []    # [cell_index: list of value]
        self.conf_set = {}          # {cell_index: set of cell_index}
        self.reductions = {}        # {cell_index: list of value}
        self.future_fc = {}         # {cell_index: list of cell_index}
        self.past_fc = {}           # {cell_index: list of cell_index}
        for key in range(len(self.cells)):
            self.original_domain[key] = copy.deepcopy(self.domain)
            self.current_domain[key] = copy.deepcopy(self.domain)
            self.conf_set[key] = {0}
            self.reductions[key] = []
            self.future_fc[key] = []
            self.past_fc[key] = [0]
        self.bcssp()

    def bcssp(self):
        i = 1
        while self.status == "unknown":
            if self.consistent:
                i = self.label(i)
            else:
                i = self.unlabel(i)
            if i >= len(self.cells):
                self.status = "solution"
            elif i == 0:
                self.status = "impossible"

    def label(self, i):
        self.consistent = False
        for val_i in self.current_domain[i]:
            if self.consistent:
                break
            self.cells[i] = val_i
            self.consistent = True
            for j in range(i+1, len(self.cells)):
                if not self.constraints[j][i]:
                    continue
                self.consistent = self.check_forward(i, j)
                if not self.consistent:
                    break
            if not self.consistent:
                if val_i in self.current_domain[i]:
                    self.current_domain[i].remove(val_i)
                self.undo_reductions(i)
                self.conf_set[i] |= set(self.past_fc[j-1])
        if self.consistent:
            return i+1
        else:
            self.cells[i] = self.domain["default"]
            return i

    def unlabel(self, i):
        h = max(max(self.conf_set[i]), max(self.past_fc[i]))
        self.conf_set[h] |= self.conf_set[i] | set(self.past_fc[i])
        self.conf_set[h].discard(h)
        for j in range(i, h, -1):   # from i downto h+1
            self.conf_set[j] = {0}
            self.undo_reductions(j)
            self.update_current_domain(j)
        self.undo_reductions(h)
        if self.cells[h] in self.current_domain[h]:
            self.current_domain[h].remove(self.cells[h])
        self.consistent = len(self.current_domain[h]) > 0
        return h

    def check_forward(self, i, j):
        reduction = []
        for val_j in self.current_domain[j]:
            self.cells[j] = val_j
            if not self.check(i, j):
                reduction.append(val_j)
        if len(reduction) > 0:
            self.current_domain[j] = [
                z for z in self.current_domain[j] if z not in reduction]
            self.reductions[j].append(reduction)
            self.future_fc[i].append(j)
            self.past_fc[j].append(i)
        self.cells[j] = self.domain["default"]
        return len(self.current_domain[j]) > 0

    def check(self, i, j):
        # [j][i] to save space by only using the left half of the matrix
        constraints = self.constraints[j][i]
        for constr in constraints:
            region = [self.cells[c] for c in constr.region]
            if not constr.check_func(region):
                return False
        return True

    def undo_reductions(self, i):
        for j in self.future_fc[i]:
            if self.reductions[j]:
                reduction = self.reductions[j].pop()
                for red in reduction:
                    if red not in self.current_domain[j]:
                        self.current_domain[j].append(red)
            if self.past_fc[j]:
                self.past_fc[j].pop()
        self.future_fc[i] = []

    def update_current_domain(self, i):
        self.current_domain[i] = copy.deepcopy(self.original_domain[i])
        for red in self.reductions[i]:
            for z in red:
                if z in self.current_domain[i]:
                    self.current_domain[i].remove(z)

# Began on 2018-08-13 by Evan Robinson
# The goal of this program is to generate instances of grid-based puzzles
# based on their rules, the values the cells of the grid can take,
# and the size of the grid
#
# The puzzle generator works as follows:
# 1. Formulate the rules of a logic puzzle constraints into a function
#    that takes an array of cells and returns a Boolean
#    if the array follows that rule
# 2. Generate the solution state of the puzzle first
# 3. While generating the solution, verify that the solution is valid
# 4. Generate the initial state
# 5. Verify that, from the start state, there is always a valid step
#    (a step that either eliminates a candidate from a cell,
#     or assigns it a value when there is one candidate remaining)
#    and that the steps (up to permutation)
#    will lead to the previously generated solution
# 6. Return the two states (either in JSON or image format)

# USAGE NOTES
# grid_type: {"rect": {"width": number, "height": number}}
# NOTE for Ch. 5, to add: "hex", "tri"
# region_spec: {"predefined"?: ["latin_square"],
#               "rowcol"?: {"rules": [constraint_type]},
#               "nei"?: {"rules": [constraint_type], "adjacency": "edge" | "vertex"},
#               "los"?: {"rules": [constraint_type], "direction": ["edge", "cell"]},
#               "pair"?: {"rules": [constraint_type]}
# type constraint_type = | lambda (cells: list): bool
#                        | lambda (cells: list): mixed
#                        (whether cells match expected_value)
#                        | lambda (cells: list, value: mixed): int
#                        (for functions counting how much of each value is in a region)
# NOTE for Ch. 5, to add:
# 1. "user_defined" constraints
# 2. "poly"?: {"rules": [constraint_type], "min_area": number, "max_area": number},
# 3. "diag" constraints
# 4. more "predefined" constraints, e.g. flood_fill
# domain: {"${name}": {"value": int or str, **kwargs}}
# clue_distrib: {"edge"?: {"proportion": float (fraction of edge clues)},
#                "cell"?: {"values": set of values, "proportion": float}
#                # NOTE at the moment, one "proportion" for the whole set of cell clues
#                "count"?: [list of values],
#                "pair"?: {"adjacency": "edge" | "vertex", "proportion": float}

import copy
import random

import clue_constr_gen
import grid_gen
import init_gen
import sol_gen


class Generator:
    def __init__(self, grid_type: dict, region_spec: dict, domain: dict, clue_distrib: dict):
        self.grid_type = grid_type
        self.region_spec = region_spec
        self.domain = domain
        self.clue_distrib = clue_distrib
        self.status = "gen_start"
        self.cells = []
        self.constraints = []
        self.clue_constraints = {}
        self.valid_assignments = {}
        self.initial = {}
        self.solution = []

        if "default" not in self.domain:
            self.status = "Please add a default cell type"

        spec_keys = {"predefined", "rowcol", "nei", "los", "pair"}
        if len(set(region_spec.keys()) - spec_keys) > 0:
            self.status = "Unknown region specification keys detected"

        if self.status == "gen_start":
            self.generate()
            print(self.__str__())

    def generate(self):
        total_attempts = 100
        attempt = 0

        if self.status == "gen_start":
            grid = grid_gen.Gridgen(
                self.grid_type, self.region_spec, self.domain)
            if grid:
                self.status = "solution_start"
                self.cells = grid.cells
                self.constraints = grid.constraints
            else:
                # if there is a problem with the specification
                # of the grid, quit immediately
                self.status = "grid_spec_problem"
                return

        while self.status != "puzzle_complete" and attempt < total_attempts:
            attempt += 1
            print("attempt", attempt)
            if self.status == "solution_start":
                solution = sol_gen.Solgen(
                    self.cells, self.constraints, self.domain)
                print("solution_status", solution.status)
                if solution.status == "solution":
                    self.solution = copy.deepcopy(solution.cells)
                    self.status = "clue_gen"

            if self.status == "clue_gen":
                csgen = clue_constr_gen.Clueconstrgen(
                    self.solution, self.grid_type, self.region_spec, self.clue_distrib, self.domain, self.constraints)
                if csgen:
                    self.clue_constraints = csgen.clue_constraints
                    self.valid_assignments = csgen.valid_assignments
                    self.status = "initial_verify"
                else:
                    # if there is a problem with the specification
                    # of the clue constraints, quit immediately
                    self.status = "clue_spec_problem"
                    return

            if self.status == "initial_verify":
                initial = init_gen.Initgen(self.domain, self.solution, self.constraints,
                                           self.clue_constraints, self.clue_distrib, self.valid_assignments)
                print("initial_status", initial.status)
                if initial.status == "solution":
                    self.initial = copy.deepcopy(initial.current_clues)
                    self.status = "puzzle_complete"
                else:
                    self.status = "solution_start"

    def __str__(self):
        status = "status " + self.status
        initial = "\n\ninitial\n"
        solution = "\nsolution"
        for key in self.initial.keys():
            for index, clue in enumerate(self.initial[key]):
                initial += str(index + 1) + " " + str(key) + " " + clue.check_func.__name__ + \
                    " " + str(clue.region) + " " + \
                    str(clue.expected_value) + "\n"
        for index in range(1, len(self.solution)):
            solution += " " + str(self.solution[index]["value"])
        return status + initial + solution


if __name__ == "__main__":
    # for timing
    import time

    # TOWERS
    def towers_constraint_1(cells):
        cell_max = 0
        total_visible = 0
        for i in range(len(cells)):
            val = cells[i]["value"]
            if val is None:
                return None
            elif val > cell_max:
                total_visible += 1
                cell_max = val
        return total_visible
    for i in range(10):
        print("\nTEST", i+1)
        elapsed = time.time()
        towers_puzzle = Generator(
            {"rect": {"width": 8, "height": 8}},
            {"predefined": ["latin_square"], "los": {
                "rules": [towers_constraint_1], "direction": ["edge"]}},
            {"default": {"value": None}, 1: {"value": 1}, 2: {"value": 2},
                3: {"value": 3}, 4: {"value": 4}, 5: {"value": 5},
                6: {"value": 6}, 7: {"value": 7}, 8: {"value": 8}},
            {"edge": {"proportion": 0.0},
            "cell": {"values": {1, 2, 3, 4, 5, 6, 7, 8}, "proportion": 0.0}}
        )
        total = time.time() - elapsed
        print("time", total)

    # UNEQUAL
    # def unequal_constraint_1(cells):
    #     if cells[0]["value"] is None or cells[1]["value"] is None:
    #         return None
    #     if cells[0]["value"] > cells[1]["value"]:
    #         return ">"
    #     else:
    #         return "<"
    # for i in range(10):
    #     print("\nTEST", i+1)
    #     elapsed = time.time()
    #     unequal_puzzle = Generator(
    #         {"rect": {"width": 8, "height": 8}},
    #         {"predefined": ["latin_square"], "pair": {
    #             "rules": [unequal_constraint_1]}},
    #         {"default": {"value": None}, 1: {"value": 1}, 2: {"value": 2},
    #             3: {"value": 3}, 4: {"value": 4}, 5: {"value": 5},
    #             6: {"value": 6}, 7: {"value": 7}, 8: {"value": 8}},
    #         {"pair": {"adjacency": "edge", "proportion": 0.0},
    #         "cell": {"values": {1, 2, 3, 4, 5, 6, 7, 8}, "proportion": 0.0}}
    #     )
    #     total = time.time() - elapsed
    #     print("time", total)

# Gridgen is divided conceptually into three parts
# generating the cells
# generating the regions
# generating the **left** half of an n x n matrix
# (n is the number of cells plus 1 as there is a null at cells[0])
# where constraint_matrix[a][b] (with a > b) is
# the list of constraints that apply to both a and b
# NOTE This program only adds the non-clue constraints
# Clue constraints are added in the program, cluespotgen.py

from collections import namedtuple
import copy


def latin_square(cells):
    cell_set = set()
    for cell in cells:
        value = cell["value"]
        if value is None:  # while cell still empty, ignore
            continue
        elif value in cell_set:
            return False
        else:
            cell_set.add(value)
    return True


class Gridgen:
    def __init__(self, grid_type, region_spec, domain):
        self.grid_type = copy.deepcopy(grid_type)
        self.region_spec = copy.deepcopy(region_spec)
        self.domain = copy.deepcopy(domain)

        self.cells = []
        self.constraints = []
        self.Constr = namedtuple("Constr", ["region", "check_func"])
        self.set_keys = set()
        for entity in self.domain.keys():
            keys = set(domain[entity].keys())
            self.set_keys |= keys

        self.generate()

    def __str__(self):
        return str(self.cells) + "\n\n" + str(self.constraints)

    def generate(self):
        if "rect" in self.grid_type:
            width = self.grid_type["rect"]["width"]
            height = self.grid_type["rect"]["height"]

            # self.cells = [[cell] * height] * width
            default = self.domain["default"]
            for _ in range(width * height + 1):
                cell = {}
                for key in self.set_keys:
                    if default[key]:
                        cell[key] = default[key]
                    else:
                        cell[key] = None
                self.cells.append(cell)

            # self.constraints = [[[]] * i for i in range(n)]
            for i in range(width * height + 1):
                constraint_row = []
                for _ in range(i):
                    constraint_row.append([])
                self.constraints.append(constraint_row)

            if "predefined" in self.region_spec:
                if "latin_square" in self.region_spec["predefined"]:
                    if "rowcol" in self.region_spec:
                        self.region_spec["rowcol"]["rules"].append(
                            latin_square)
                    else:
                        self.region_spec["rowcol"] = {"rules": [latin_square]}

            if "rowcol" in self.region_spec:
                for r in range(height):
                    row = [r * width + p + 1 for p in range(width)]
                    self.constraint_gen("rowcol", row)
                for c in range(width):
                    col = [q * width + c + 1 for q in range(height)]
                    self.constraint_gen("rowcol", col)

            if "nei" in self.region_spec:
                neighbors = []
                adjacency = self.region_spec["nei"]["adjacency"]
                if adjacency == "edge":
                    neighbors = [(0, -1), (-1, 0), (0, 1), (1, 0)]
                elif adjacency == "vertex":
                    neighbors = [(-1, -1), (0, -1), (1, -1),
                                 (0, -1), (0, 1), (1, -1),
                                 (1, 0), (1, 1)]
                for row in range(height):
                    for col in range(width):
                        region = []
                        for nei in neighbors:
                            coord_y = row + nei[0]
                            coord_x = col + nei[1]
                            if coord_x >= 0 and coord_y >= 0:
                                region.append(coord_y * width + coord_x + 1)
                        self.constraint_gen("nei", region)

    def constraint_gen(self, keyword, region):
        for check_func in self.region_spec[keyword]["rules"]:
            new_constr = self.Constr(region, check_func)
            for p in range(len(region)):
                for q in range(p+1, len(region)):
                    i = region[p]
                    j = region[q]
                    a = max(i, j)
                    b = min(i, j)
                    # [a][b] where a > b so that we only use the left half
                    self.constraints[a][b].append(new_constr)
